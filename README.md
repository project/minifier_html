## Minifier HTML


Minifier HTML module for Drupal 8 community users

- Minify HTML and any CSS or JS included in your markup.
- Compress your site's HTML and it help to reduce your size of your rendered output.

## How to use the module

- Just install the module no settings are required to compress the HTML.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- Alok Narwaria - [aloknarwaria](https://www.drupal.org/u/aloknarwaria)
- JEET PATEL - [jeetmail72](https://www.drupal.org/u/jeetmail72)
- Malay Nayak - [malaynayak](https://www.drupal.org/u/malaynayak)
- Harshita Saini - [harshita29](https://www.drupal.org/u/harshita29)
- Sarthak - [sarthak drupal](https://www.drupal.org/u/sarthak-drupal)